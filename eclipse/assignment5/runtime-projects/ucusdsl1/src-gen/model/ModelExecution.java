package model;

import roles.*;
import factories.*;
		
public class ModelExecution {

	public static void execute(Factory factory) {
		// Setup default factory
		SFactory.setInstance(factory);

		// Execute the roles...
		executeRoles();
	}

	private static void executeRoles() {
		boolean exit = false;

		java.util.Scanner in = new java.util.Scanner(System.in);

		while (!exit) {
			System.out.println("# Roles #");
			System.out.println("Please select option:");

			System.out.println("0- Exit");
			System.out.println("1- Customer");
			System.out.println("2- Admin");
			System.out.println("3- Guest");
			System.out.println("4- AnotherUser");

			// Read an integer from the input
			int num = in.nextInt();

			switch (num) {
				case 0:
					exit = true;
					break;
				case 1: {
					Role role = SFactory.getInstance().getCustomer();
					role.execute();
				}
				break;
				case 2: {
					Role role = SFactory.getInstance().getAdmin();
					role.execute();
				}
				break;
				case 3: {
					Role role = SFactory.getInstance().getGuest();
					role.execute();
				}
				break;
				case 4: {
					Role role = SFactory.getInstance().getAnotherUser();
					role.execute();
				}
				break;
			}
		}
	}
}			 
