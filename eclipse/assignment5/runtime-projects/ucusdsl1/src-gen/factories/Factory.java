package factories; 

import roles.*;
import interactions.*;
		
public interface Factory {
	
	public Role getCustomer();
	public Role getAdmin();
	public Role getGuest();
	public Role getAnotherUser();
	public Interaction getReviewProduct();
	public Interaction getCommentProduct();
	public Interaction getInsertPicture();
}

