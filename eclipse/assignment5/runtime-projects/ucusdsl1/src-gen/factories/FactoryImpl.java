package factories; 

import roles.*;
import interactions.*;
		
public class FactoryImpl implements Factory {
	
	public Role getCustomer() {
		return new Customer();
		}
	public Role getAdmin() {
		return new Admin();
		}
	public Role getGuest() {
		return new Guest();
		}
	public Role getAnotherUser() {
		return new AnotherUser();
		}
	public Interaction getReviewProduct() {
		return new ReviewProduct();
		}
	public Interaction getCommentProduct() {
		return new CommentProduct();
		}
	public Interaction getInsertPicture() {
		return new InsertPicture();
		}
}

