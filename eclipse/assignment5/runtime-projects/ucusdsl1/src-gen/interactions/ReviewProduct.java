package interactions;

import factories.*;

public class ReviewProduct implements Interaction {
	
	@Override
	public void execute() {
		
		System.out.println("ReviewProduct: Please add behaviour code");
		
		{
			// Calling included use case
			Interaction inter=SFactory.getInstance().getInsertPicture();
			inter.execute();
		}
	}			
}
