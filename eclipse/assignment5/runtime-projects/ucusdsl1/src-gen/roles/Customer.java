package roles;

import interactions.Interaction;
import factories.SFactory;

public class Customer implements Role {
	
	@Override
	public void execute() {
		boolean exit=false;

		java.util.Scanner in = new java.util.Scanner(System.in);

		while (!exit) {		
			System.out.println("# Customer #");
			System.out.println("Please select option:");
	
			System.out.println("0- Exit");
			System.out.println("1- ReviewProduct (Amazon)");
			System.out.println("2- CommentProduct (Amazon)");
			// Read an integer from the input
			int num = in.nextInt();

			switch (num) {
			case 0:
				exit=true;
				break;
			case 1:
				{
					Interaction inter=SFactory.getInstance().getReviewProduct();
					inter.execute();
				}
				break;
			case 2:
				{
					Interaction inter=SFactory.getInstance().getCommentProduct();
					inter.execute();
				}
				break;
			}		
		}		
	}			
}
