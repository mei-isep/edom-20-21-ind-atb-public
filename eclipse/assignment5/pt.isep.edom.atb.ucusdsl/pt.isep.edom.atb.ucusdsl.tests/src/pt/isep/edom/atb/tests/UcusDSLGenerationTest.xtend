package pt.isep.edom.atb.tests

import org.eclipse.xtext.testing.InjectWith
import com.google.inject.Inject
import org.junit.jupiter.api.Test
import org.eclipse.xtext.xbase.testing.CompilationTestHelper
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
//@InjectWith(UcusDSLInjectorProvider)
@InjectWith(MyUcusDSLInjectorProvider)
class UcusDSLGenerationTest {
	
	@Inject extension CompilationTestHelper
	
	@Test def void testGeneratedJavaCodeSample1() {
		'''
		Model amazon {
			subject {
				Subject Amazon {
					usecase {
						UseCase ReviewProduct {	
							includes {
								Include inc {
									addition InsertPicture
								}
							}			 	
						}
						, UseCase CommentProduct {}
						, UseCase InsertPicture {}        
					}  
				}     
			}	   
			actor {   
				Actor Customer {}
				, Actor Admin {}
				, Actor Guest {}
				, Actor AnotherUser2 {}
			}
			association {
				Association asso1 {
					actor Customer usecase "Amazon.ReviewProduct"
				}, 
				Association asso2 {
					actor Customer usecase "Amazon.CommentProduct"
				}
			}
		}		
		'''.compile[
			val allErrors = getErrorsAndWarnings.filter[severity == Severity.ERROR]
			if (!allErrors.empty) {
				throw new IllegalStateException(
					"One or more resources contained errors : " + allErrors.map[toString].join(", ")
 				); 
 			}
 			
// Example			
//			val FirstEntity=getCompiledClass("entities.FirstEntity").newInstance
//			val SecondEntity=getCompiledClass("entities.SecondEntity").newInstance
//			SecondEntity.invoke("setS", "testvalue")  
//			FirstEntity.invoke("setMyAttribute", SecondEntity) 
//			SecondEntity.assertSame(FirstEntity.invoke("getMyAttribute")) 
//			"testvalue".assertEquals(FirstEntity.invoke("getMyAttribute").invoke("getS"))
			]
		}
}