package pt.isep.edom.atb.ucusdsl.proto1;

public interface Factory {
	public Interaction getReviewProduct();
	public Interaction getInsertPicture();
	public Role getUser();
}
