package pt.isep.edom.atb.ucusdsl.proto1;

public interface Interaction {
	public void execute();
}
