/**
 */
package pt.isep.edom.atb.ucus.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import pt.isep.edom.atb.ucus.UcusFactory;
import pt.isep.edom.atb.ucus.UseCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getDescription() <em>Description</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getAllIncludedUseCases() <em>All Included Use Cases</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getSubject() <em>Subject</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UseCaseTest extends TestCase {

	/**
	 * The fixture for this Use Case test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCase fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UseCaseTest.class);
	}

	/**
	 * Constructs a new Use Case test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Use Case test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UseCase fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Use Case test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCase getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UcusFactory.eINSTANCE.createUseCase());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link pt.isep.edom.atb.ucus.UseCase#getDescription() <em>Description</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.isep.edom.atb.ucus.UseCase#getDescription()
	 * @generated
	 */
	public void testGetDescription() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link pt.isep.edom.atb.ucus.UseCase#getAllIncludedUseCases() <em>All Included Use Cases</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.isep.edom.atb.ucus.UseCase#getAllIncludedUseCases()
	 * @generated
	 */
	public void testGetAllIncludedUseCases() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link pt.isep.edom.atb.ucus.UseCase#getSubject() <em>Subject</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see pt.isep.edom.atb.ucus.UseCase#getSubject()
	 * @generated
	 */
	public void testGetSubject() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UseCaseTest
