/**
 */
package pt.isep.edom.atb.ucus;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.isep.edom.atb.ucus.Model#getSubject <em>Subject</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.Model#getActor <em>Actor</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.Model#getName <em>Name</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.Model#getAssociation <em>Association</em>}</li>
 * </ul>
 *
 * @see pt.isep.edom.atb.ucus.UcusPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject {
	/**
	 * Returns the value of the '<em><b>Subject</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Subject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' containment reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getModel_Subject()
	 * @model containment="true"
	 * @generated
	 */
	EList<Subject> getSubject();

	/**
	 * Returns the value of the '<em><b>Actor</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Actor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actor</em>' containment reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getModel_Actor()
	 * @model containment="true"
	 * @generated
	 */
	EList<Actor> getActor();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getModel_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pt.isep.edom.atb.ucus.Model#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Association</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Association}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' containment reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getModel_Association()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociation();

} // Model
