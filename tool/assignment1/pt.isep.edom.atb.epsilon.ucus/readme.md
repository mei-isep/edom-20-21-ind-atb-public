# Instructions

## 1- Edit teh metamodel using Emfatic at models/ucus.emf

## 2- Export the Emfatic to ecore by using right click in ucus.emf and selecting "Generate Ecore Model"

## 3- Create a new instance (model) by opening the ucus.ecore (with "Sample Ecore Model Editor"), selecting "Model", right cliek and selecting "Create Dynamic Instance". Select "ucus1.xmi" as its name.

## 4- Open ucus1.xmi using "Sample Reflective Ecore Model Editor"

## 5- Add use cases and actors

## 6- Open (and edit) the ucus.eol file. It uses EOL to create instances of concepts in a ucus model

## 7- You can run the previous ucus.eol using the ucus.launch file. The launch file should automatically be read by Eclipse and should be available in the menu "Run/Run Configurations" as "EOL Program"

## 8- Complete the ucus metamodel as instructed in the lecture 
